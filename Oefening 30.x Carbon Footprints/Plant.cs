﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_30.x_Carbon_Footprints
{
    class Plant : ICarbonFootprint
    {
        private int _carbonfootprint;

        public Plant() 
        { 
            _carbonfootprint = 0;
        }

        public int CarbonFootprint 
        { 
            get { return _carbonfootprint; } 
            set { _carbonfootprint = value; } 
        }

        public int BerekenFootprint()
        {
            return _carbonfootprint;
        }

        public void VerlaagFootprint()
        {
            _carbonfootprint = 0;
        }
    }
}
