﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_30.x_Carbon_Footprints
{
    interface ICarbonFootprint
    {
        int BerekenFootprint();
        void VerlaagFootprint();
    }
}
