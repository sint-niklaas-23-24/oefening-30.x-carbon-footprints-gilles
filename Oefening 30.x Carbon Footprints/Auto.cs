﻿using System;

namespace Oefening_30.x_Carbon_Footprints
{
    class Auto : ICarbonFootprint
    {
        private string _merk;
        private int _bouwjaar;
        private string _model;
        private int _carbonFootprint;


        public Auto() { }
        public Auto(string merk, string model, int bouwjaar)
        {
            Merk = merk;
            Model = model;
            Bouwjaar = bouwjaar;
            CarbonFootprint = BerekenFootprint();
        }

        public string Merk
        {
            get { return _merk; }
            set { _merk = value; }
        }
        public int Bouwjaar
        {
            get { return _bouwjaar; }
            set { _bouwjaar = value; }
        }
        public string Model
        {
            get { return _model; }
            set { _model = value; }
        }
        public int CarbonFootprint
        {
            get { return _carbonFootprint; }
            set { _carbonFootprint = value; }
        }

        public int BerekenFootprint()
        {
            //waarden zijn CO2/km
            int carbonfootprint;
            switch (Merk)
            {
                case "Mercedes":
                    carbonfootprint = 137;
                    break;
                case "Ford":
                    carbonfootprint = 131;
                    break;
                case "Volvo":
                    carbonfootprint = 163;
                    break;
                case "Volkswagen":
                    carbonfootprint = 141;
                    break;
                case "Opel":
                    carbonfootprint = 99;
                    break;
                case "Peugeot":
                    carbonfootprint = 120;
                    break;
                case "Citroën":
                    carbonfootprint = 146;
                    break;
                case "Renault":
                    carbonfootprint = 95;
                    break;
                case "BMW":
                    carbonfootprint = 289;
                    break;
                case "Audi":
                    carbonfootprint = 170;
                    break;
                case "Land Rover":
                    carbonfootprint = 209;
                    break;
                case "Jeep":
                    carbonfootprint = 128;
                    break;
                case "Skoda":
                    carbonfootprint = 114;
                    break;
                case "Mazda":
                    carbonfootprint = 144;
                    break;
                case "Hyundai":
                    carbonfootprint = 114;
                    break;
                case "Mitsubishi":
                    carbonfootprint = 110;
                    break;
                case "Ferrari":
                    carbonfootprint = 270;
                    break;
                case "Maserati":
                    carbonfootprint = 207;
                    break;
                case "Lamborghini":
                    carbonfootprint = 332;
                    break;
                case "Porsche":
                    carbonfootprint = 256;
                    break;
                case "Jaguar":
                    carbonfootprint = 196;
                    break;
                case "Tesla":
                    carbonfootprint = 16;
                    break;
                default:
                    carbonfootprint = 150;
                    break;

            }
            CarbonFootprint = carbonfootprint;
            return CarbonFootprint;
        }
        public void VerlaagFootprint()
        {
            this.CarbonFootprint -= this.CarbonFootprint / 4;
        }
        public override string ToString()
        {
            return $"De carbon voetadruk van een {Merk} auto is {CarbonFootprint}g/km.";
        }
    }
}
