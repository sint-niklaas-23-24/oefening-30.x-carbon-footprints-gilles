﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Oefening_30.x_Carbon_Footprints
{
    class Huis : ICarbonFootprint
    {
        private int _breedte;
        private int _hoogte;
        private int _diepte;
        private int _carbonFootprint;


        public Huis() { }
        public Huis(int breedte, int hoogte, int diepte)
        {
            Breedte = breedte;
            Hoogte = hoogte;
            Diepte = diepte;
            CarbonFootprint = BerekenFootprint();
        }

        public int Breedte
        {
            get { return _breedte; } 
            set 
            { 
                if (value > 0)
                {
                    _breedte = value;
                } else
                {
                    throw new FormatException("De breedte moet een getal zijn groter dan 0.");
                }
            } 
        }
        public int Hoogte
        {
            get { return _hoogte; }
            set 
            { 
                if (value > 0)
                {
                    _hoogte = value;
                }
                else
                {
                    throw new FormatException("De hoogte moet een getal zijn groter dan 0.");
                }
            }
        }
        public int Diepte
        {
            get { return _diepte; }
            set 
            { 
                if (value > 0)
                {
                    _diepte = value;
                }
                else
                {
                    throw new FormatException("De diepte moet een getal zijn groter dan 0.");
                }
            }
        }
        public int CarbonFootprint
        {
            get { return _carbonFootprint; }
            set { _carbonFootprint = value; }
        }


        public int BerekenVolume()
        {
            return Hoogte * Breedte * Diepte;
        }
        public int BerekenFootprint()
        {
            return CarbonFootprint = BerekenVolume() * 10;
        }

        public void VerlaagFootprint()
        {
            this.CarbonFootprint = BerekenVolume() * 9;
        }
        public override string ToString()
        {
            return $"De carbon voetadruk van het huis met volume {BerekenVolume()} is {CarbonFootprint} CO2/m³.";
        }
    }
}
