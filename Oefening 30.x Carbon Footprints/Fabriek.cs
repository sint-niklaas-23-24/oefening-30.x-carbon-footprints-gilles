﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_30.x_Carbon_Footprints
{
    class Fabriek : ICarbonFootprint
    {
        private int _aantalWerknemers;
        private int _carbonFootprint;

        public Fabriek() { }
        public Fabriek(int aantalWerknemers)
        {
            AantalWerknemers = aantalWerknemers;
            CarbonFootprint = BerekenFootprint();
        }

        public int AantalWerknemers
        {
            get { return _aantalWerknemers; }
            set { _aantalWerknemers |= value; }
        }
        public int CarbonFootprint
        {
            get { return _carbonFootprint; }
            set { _carbonFootprint = value; }
        }

        public int BerekenFootprint()
        {
            return CarbonFootprint = AantalWerknemers * 100;
        }

        public void VerlaagFootprint()
        {
            this.CarbonFootprint -= this.CarbonFootprint / 2;
        }
        public override string ToString()
        {
            return $"De carbon voetadruk van de fabriek is {CarbonFootprint} CO2 voor alle werknemers.";
        }
    }
}
