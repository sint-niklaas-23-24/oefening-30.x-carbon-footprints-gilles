﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Documents;

namespace Oefening_30.x_Carbon_Footprints
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            int jaar = 2024;
            foreach (string merknaam in arrAutomerknamen)
            {
                cmbMerken.Items.Add(merknaam);
            }
            for (int i = 0; i < 54; i++)
            {
                arrBouwjaren[i] = jaar;
                jaar--;
                cmbBouwjaar.Items.Add(jaar);
            }
            Plant plant1 = new Plant();
            Plant plant2 = new Plant();
            Auto auto1 = new Auto("Peugeot", "G2", 2006);
            Auto auto2 = new Auto("Ford", "Focus", 2009);
            Fabriek fabriek1 = new Fabriek(10);
            Fabriek fabriek2 = new Fabriek(5);
            Huis huis1 = new Huis(10, 5, 2);
            Huis huis2 = new Huis(2, 5, 2);
            lijstCarbonFootprint.Add(plant1);
            lijstCarbonFootprint.Add(plant2);
            lijstCarbonFootprint.Add(auto1);
            lijstCarbonFootprint.Add(auto2);
            lijstCarbonFootprint.Add(fabriek1);
            lijstCarbonFootprint.Add(fabriek2);
            lijstCarbonFootprint.Add(huis1);
            lijstCarbonFootprint.Add(huis2);
            cmbMerken.SelectedIndex = 0;
            cmbBouwjaar.SelectedIndex = 0;
        }
        string info = "";
        string[] arrAutomerknamen = { "Mercedes", "Ford", "Volvo", "Volkswagen", "Opel", "Peugeot", "Citroën", "Renault", "BMW", "Audi", "Land Rover", "Jeep", "Skoda", "Mazda", "Hyundai", "Mitsubishi", "Ferrari", "Maserati", "Lamborghini", "Porsche", "Jaguar", "Tesla", "Andere" };
        int[] arrBouwjaren = new int[54];
        List<ICarbonFootprint> lijstCarbonFootprint = new List<ICarbonFootprint>();
        private void btnAutoAanmaken_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(txtModel.Text))
                {
                    string merk = cmbMerken.SelectedItem.ToString();
                    int bouwjaar = Convert.ToInt32(cmbBouwjaar.SelectedItem);
                    Auto auto = new Auto(merk, txtModel.Text, bouwjaar);
                    info = $"TOEGEVOEGDE AUTO:{Environment.NewLine}Merk: {auto.Merk}{Environment.NewLine}Model: {auto.Model}{Environment.NewLine}Bouwjaar: {auto.Bouwjaar}";
                    txtOverzichtAuto.Text = info;
                    lijstCarbonFootprint.Add(auto);
                }
                else
                {
                    throw new FormatException("Gelieve alle velden in te vullen.");
                }
            }
            catch (FormatException fe)
            {
                MessageBox.Show(fe.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Er ging iets mis.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnFabriekAanmaken_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(txtAantalWerknemers.Text))
                {
                    int aantalWerknemers = Convert.ToInt32(txtAantalWerknemers.Text);
                    Fabriek fabriek = new Fabriek(aantalWerknemers);
                    info = $"TOEGEVOEGDE FABRIEK: {Environment.NewLine}Aantal werknemers: {fabriek.AantalWerknemers}";
                    txtOverzichtFabriek.Text = info;
                    lijstCarbonFootprint.Add(fabriek);
                }
                else
                {
                    throw new FormatException("Gelieve alle velden in te vullen.");
                }
            }
            catch (FormatException fe)
            {
                MessageBox.Show(fe.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Er ging iets mis.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnHuisAanmaken_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(txtBreedte.Text) && !String.IsNullOrEmpty(txtHoogte.Text) && !String.IsNullOrEmpty(txtDiepte.Text))
                {
                    int breedte = Convert.ToInt32(txtBreedte.Text);
                    int hoogte = Convert.ToInt32(txtHoogte.Text);
                    int diepte = Convert.ToInt32(txtDiepte.Text);
                    Huis huis = new Huis(breedte, hoogte, diepte);
                    info = $"TOEGEVOEGD HUIS: {Environment.NewLine}Breedte: {huis.Breedte}{Environment.NewLine}Hoogte: {huis.Hoogte}{Environment.NewLine}Diepte: {huis.Diepte}";
                    txtOverzichtHuis.Text = info;
                    lijstCarbonFootprint.Add(huis);
                }
                else
                {
                    throw new FormatException("Gelieve alle velden in te vullen.");
                }
            }
            catch (FormatException fe)
            {
                MessageBox.Show(fe.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Er ging iets mis.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnFootprintWeergeven_Click(object sender, RoutedEventArgs e)
        {
            string resultaat = "";
            int index = 0;
            foreach (ICarbonFootprint obj in lijstCarbonFootprint)
            {
                if (obj.GetType() != typeof(Plant))
                {
                    resultaat += obj.ToString() + Environment.NewLine; 
                }
            }
            MessageBox.Show(resultaat, "Carbonfootprints", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void btnGemiddeldeWeergeven_Click(object sender, RoutedEventArgs e)
        {
            int gemiddelde = 0;
            int totaal = 0;
            foreach (ICarbonFootprint obj in lijstCarbonFootprint)
            {
                totaal += obj.BerekenFootprint();
            }
            gemiddelde = totaal / lijstCarbonFootprint.Count;
            MessageBox.Show($"De gemiddelde carbonfootprint van alle objecten bedraagt {gemiddelde} CO2/object.", "Gemiddelde objecten", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void btnHoogsteWeergeven_Click(object sender, RoutedEventArgs e)
        {
            ICarbonFootprint hoogsteObject = null; 
            int hoogstegetal = -1;
            foreach (ICarbonFootprint obj in lijstCarbonFootprint)
            {
                int waarde = Convert.ToInt32(obj.GetType().GetProperty("CarbonFootprint").GetValue(obj));
                if (hoogstegetal < waarde)
                {
                    hoogstegetal = waarde;
                    hoogsteObject = obj;
                }
            }
            if (hoogsteObject.GetType() == typeof(Auto))
            {
                MessageBox.Show($"De hoogste carbonfootprint van alle objecten is:{Environment.NewLine}{hoogsteObject.GetType().GetProperty("Merk").GetValue(hoogsteObject)} met een waarde van {hoogsteObject.BerekenFootprint()} CO2/km.", "Hoogste footprint", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else if (hoogsteObject.GetType() == typeof(Fabriek))
            {
                MessageBox.Show($"De hoogste carbonfootprint van alle objecten is:{Environment.NewLine}{hoogsteObject.GetType().Name} ({hoogsteObject.GetType().GetProperty("AantalWerknemers").GetValue(hoogsteObject)} werknemers) met een waarde van {hoogsteObject.BerekenFootprint()} CO2 voor alle werknemers.", "Hoogste footprint", MessageBoxButton.OK, MessageBoxImage.Information);
            } 
            else
            {
                MessageBox.Show($"De hoogste carbonfootprint van alle objecten is:{Environment.NewLine}{hoogsteObject.GetType().Name} met een waarde van {hoogsteObject.BerekenFootprint()} CO2/m³.", "Hoogste footprint", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnFootprintVerlagen_Click(object sender, RoutedEventArgs e)
        {
            foreach (ICarbonFootprint obj in lijstCarbonFootprint)
            {
                obj.VerlaagFootprint();
            }
        }
    }
}
